FROM registry.gitlab.com/sbenv/veroxis/images/sdks/zig:0.13.0 as zig
FROM registry.gitlab.com/sbenv/veroxis/images/macosx-sdks:13.0 as macosx-sdk
FROM golang:1.23.0-alpine as golang

# ---

FROM golang:1.23.0-alpine

RUN apk add --no-cache "build-base" "meson" "cmake" "pkgconfig" "git" "curl" "wget" "jq" "yq" "python3" "curl-dev" "openssl-dev" "libpq-dev" "sqlite-dev" "zlib-dev"

COPY --from=zig /usr/bin/zig /usr/bin/zig
COPY --from=zig /usr/lib/zig /usr/lib/zig
COPY --from=zig /usr/share/licenses/zig/LICENSE /usr/share/licenses/zig/LICENSE
RUN chmod -R g+w,o+w /usr/lib/zig

COPY --from=macosx-sdk /opt/MacOSX.sdk /opt/MacOSX.sdk
ENV SDKROOT=/opt/MacOSX.sdk
