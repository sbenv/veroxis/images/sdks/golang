#!/bin/sh

sed '/^FROM golang/!d' Dockerfile | tail -n 1 | cut -d':' -f2 | cut -d'-' -f1
